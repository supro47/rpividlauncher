const express = require('express')
const http = require('http')
const path = require('path')
const socketIO = require('socket.io')
const walk = require('walk')

const publicPath = path.join(__dirname, '/public')

const port = 3000

const expApp = express()
const server = http.createServer(expApp)
const io = socketIO(server)

const usbDir = "/mnt/usb"


let videoList = []
let vidID = 0;

expApp.use(express.static(publicPath))
expApp.use('/files', express.static(usbDir))

io.on('connection', (socket) => {
    console.log('ready')
    fileWalker(socket)

    socket.on('video:load', (id) => {
        let video = videoList[id].path
        socket.emit('todo:add', video)
    })
})

fileWalker = (socket) => {
    videoList = []
    vidID = 0;
    const walker = walk.walk(usbDir, { followLinks: false })
    walker.on('file', function(root, stat, next) {
        const re = /(?:\.([^.]+))?$/
        const ext = re.exec(stat.name)[1]
        const vidPath = root.replace(usbDir, "files") + '/' + stat.name
        if ((ext == "mp4") || (ext == "mkv") || (ext == "mov") || (ext == "avi")) {
            videoList.push({
                id: vidID,
                file: stat.name,
                path: vidPath
            })
            vidID++
        }

        next()
    })
    
    walker.on('end', function() {
        socket.emit('video:renderlist', videoList)
    })
}

server.listen(port, () => {
    console.log(`server is up on ${port}`)
})